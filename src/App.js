import { useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import Home from './homepage/home';
import Nav from './layout/nav';
import WOW from 'wowjs';

function App() {
  useEffect(()=>{
    new WOW.WOW({
        live: false
    }).init();
},[])
  return (
    <div className="App">
      <Nav/>
      <Home/>
    </div>
  );
}

export default App;
