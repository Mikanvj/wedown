import react from "react";
import styles from "./nav.module.css";
import logo from "../img/6.png";

const Nav = ()=>{
    return(
        <>
        <div className={styles.nav}>
            <h2 className={styles.titre}>W2D Downloader </h2>
            <img src={logo} width="40px" className={styles.logo}/>
        </div>
        
        </>
    )
}
export default Nav;