import React, { useState, useEffect } from "react";
import styles from "./home.module.css";
import { Button, TextField } from "@material-ui/core";
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const Home = () => {

    const [age, setAge] = React.useState('');
    const [url, seturl] = React.useState('');
    const [sst, setSst] = useState(1);
    const [step, setStep] = useState(0);

    const handleChange = (event) => {
        setAge(event.target.value);
    };
    const handleChan = (event) => {
        seturl(event.target.value);
    };

    // var convertBtn = document.querySelector('.convert-button');

    // var URLinput = document.querySelector('.URL-input');
    // convertBtn.addEventListener('click', () => {
    //     console.log(`URL: ${URLinput.value}`);
    //     sendURL(URLinput.value);
    // });

    // function clikeo(){
    //     var url = document.getElementById("url").value;
    //     console.log(`URL: ${url}`);
    //     sendURL(url);
    // }
    // function sendURL(URL) {
    //     fetch(`http://localhost:4000/server.js?URL=${URL}`, {
    //         method: 'GET'
    //     }).then(res => res.json())
    //         .then(json => console.log(json));
    // }
    // $(".click-btn-down").click(function(){
    //     var link = $(".link").val();
    //   var fromate = $(".formte").children("option:selected").val();
    //   var src =""+link+"="+fromate+"";
    //   downloadVideo(link,fromate);
    // });
    // function downloadVideo(link,fromate) {
    //     $('.download-video').html('<iframe style="width:100%;height:60px;border:0;overflow:hidden;" scrolling="no" src="https://loader.to/api/button/?url='+link+'&f='+fromate+'"></iframe>');
    // }
    function down(lien, format) {
        var vid = document.getElementById('dow');
        vid.innerHTML = '<iframe style="width:100%;height:90%;border:0;overflow:hidden;" scrolling="no" src="https://loader.to/api/button/?url=' + lien + '&f=' + format + '"></iframe>';
    }
    function clickeo(e) {
        e.preventDefault();
        var lien = url;
        var format = age;
        setSst(2);
        down(lien, format);
    }
    function steper(i){
        setStep(i);

    }


    return (
        <>
            <div className={styles.contenair}>
                <div>
                    <h1 className={styles.title}  data-aos="fade-up">Download Your Video Here</h1>
                    <div className={styles.cont}>
                        {/* <div className={styles.down} >
                            {sst ===2 && <p id="p" className={styles.do}>click here :</p>}
                            <div id="dow">

                            </div>
                        </div>
                        <form >
                            <TextField label="URL Video" id="url" variant="outlined" />
                            <Box sx={{ minWidth: 120 }}>
                            <FormControl fullWidth>
                                <InputLabel id="demo-simple-select-label">Format</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={age}
                                    label="Format"
                                    onChange={handleChange}
                                >
                                    <MenuItem value="mp3">Mp3</MenuItem>
                                    <MenuItem value="mp4a">144 Mp4</MenuItem>
                                    <MenuItem value="360">360 Mp4</MenuItem>
                                    <MenuItem value="480">480 Mp4</MenuItem>
                                    <MenuItem value="720">720 Mp4</MenuItem>
                                    <MenuItem value="1080">1080 Mp4</MenuItem>
                                    <MenuItem value="4k">4k Mp4</MenuItem>
                                    <MenuItem value="8k">8k Mp4</MenuItem>
                                </Select>
                            </FormControl>
                        </Box>
                            <Button variant="contained" onClick={clickeo} className={styles.btn}>Download</Button>
                        </form> */}
                        {step === 0 && <div className={styles.one}>
                            <div className={`wow slideInLeft ${styles.leftone}`}>

                            </div>
                            <div className={`wow slideInRight ${styles.rightone}`}>
                                <TextField label="URL Video" className={styles.qr} required id="url" onChange={handleChan} variant="outlined" />
                                <Button variant="contained" onClick={()=>steper(2)} className={styles.btn}>Next</Button>

                            </div>
                        </div>}

                        {step === 2 &&<div className={styles.two}>
                            <div className={`wow slideInLeft ${styles.lefttwo}`}>

                            </div>
                            <div className={styles.righttwo}>
                                <Box sx={{ minWidth: 120 }}>
                                    <FormControl fullWidth>
                                        <InputLabel id="demo-simple-select-label">Format</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={age}
                                            label="Format"
                                            onChange={handleChange}
                                            required
                                        >
                                            <MenuItem value="mp3">Mp3</MenuItem>
                                            <MenuItem value="mp4a">144 Mp4</MenuItem>
                                            <MenuItem value="360">360 Mp4</MenuItem>
                                            <MenuItem value="480">480 Mp4</MenuItem>
                                            <MenuItem value="720">720 Mp4</MenuItem>
                                            <MenuItem value="1080">1080 Mp4</MenuItem>
                                            <MenuItem value="4k">4k Mp4</MenuItem>
                                            <MenuItem value="8k">8k Mp4</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Box>
                                <Button variant="contained" onClick={()=>steper(3)} className={styles.btn}>Next</Button>

                            </div>
                        </div>}

                        {step === 3 &&<div className={styles.three}>
                            <div className={styles.leftthree}>
                            
                            </div>
                            <div className={styles.rightthree} id="dow">
                                <Button variant="contained" onClick={clickeo} className={styles.btn}>Ready?</Button>
                                <div  id="dow">
                                
                                </div>
                            </div>
                        </div>}
                    </div>
                </div>
            </div>
        </>
    )
}

export default Home;